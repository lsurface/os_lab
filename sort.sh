#!/bin/bash

arr=(5 9 6 7 8 1 4 3)

echo ${arr[@]}

echo -ne "Sorted array is: "
echo $(printf "%d \n" "${arr[@]}" |sort -n)

echo -ne "Maximum element in array: "
echo $(printf "%d \n" "${arr[@]}" |sort -n|tail -n1)

echo -ne "Minimum element in array: "
echo $(printf "%d \n" "${arr[@]}" |sort -n|head -n1)
