#!/bin/bash

echo "Enter number of rows"

read n

for((i=0;i<n;i++))
do

	num_space=$(($n - $i))

	num_of_digits=$((2*$i + 1))

	half=$(($num_of_digits / 2))

	for((j=0;j<num_space;j++))
	do
		echo -ne " "
	done

	for((j=0;j<half;j++))
	do
		echo -ne $(($j + 1))
	done

	echo -ne $(($i + 1))

	for((j=half;j>0;j--))
	do
		echo -ne $j
	done

	echo ""
done
