#!/bin/bash

arr=(5 9 6 7 8 3 2 4)
echo "Display all elements: ${arr[@]}"


echo "Display 1st element: ${arr[0]}"

echo "Total number of elements: ${#arr[@]}"

echo "Replace 7 by 10: ${arr[@]/7/10}"

echo "Display elements except 1st: ${arr[@]:1}"

echo "Display elements from index 1 to 4: ${arr[@]:1:4}"

min=${arr[0]}
max=${arr[0]}

for i in ${arr[@]}
do
	if [ $i -lt $min ]
	then
		min=$i
	fi
	
	if [ $i -gt $max ]
	then
		max=$i
	fi
done

echo "Minimum is $min and Maximum is $max"

echo "Bubblesort"

for((i=0;i<${#arr[@]}-1;i++))
do
	for((j=0;j<${#arr[@]}-i-1;j++))
	do
		if [ ${arr[j]} -gt ${arr[j+1]} ]
		then
			temp=${arr[j]}
			arr[j]=${arr[j+1]}
			arr[j+1]=$temp
		fi
	done
done

echo ${arr[@]}
