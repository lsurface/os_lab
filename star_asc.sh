#!/bin/bash

echo "Enter value for n"

read n

for((i=n;i>0;i--))
do
	space=$(($n - $i))
	for((j=0;j<space;j++))
	do
		echo -ne " "
	done
	
	for((k=space;k<n;k++))
	do
		echo -ne "*"
	done

	echo ""
done
