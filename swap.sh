#!/bin/bash

echo "Enter values for a and b"

read a
read b

echo "a is $a and b is $b"

temp=$a
a=$b
b=$temp

echo "After swapping, a is $a and b is $b"
