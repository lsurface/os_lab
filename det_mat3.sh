#!/bin/bash

echo "Enter values for 3x3 matrix"

declare -a mat[9]

for((i=0;i<3;i++))
do
	for((j=0;j<3;j++))
	do
		read mat[$((3*$i+$j))]
	done

done

v1=$((${mat[0]}*(${mat[4]}*${mat[8]}-${mat[5]}*${mat[7]})))
v2=$((${mat[1]}*(${mat[3]}*${mat[8]}-${mat[5]}*${mat[6]})))
v3=$((${mat[2]}*(${mat[3]}*${mat[7]}-${mat[4]}*${mat[6]})))

echo "Matrix is"

for((i=0;i<3;i++))
do
	for((j=0;j<3;j++))
	do
		echo -n "${mat[$((3*$i+$j))]} "
	done
	echo ""
done
det=$(($v1-$v2+$v3))

echo "Determinant of matrix is $det"
