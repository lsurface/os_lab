#!/bin/bash

echo "Number of elements"
read n

echo "Enter array values"

for((i=0;i<n;i++))
do
    read arr[$i]
done

#Sorting
for((i=1;i<n;i++))
do
    j=$i-1
    temp=${arr[$i]}

    while((j>=0 && arr[j]>temp))
    do
        arr[$j+1]=${arr[$j]}
        j=$j-1
    done

    arr[j+1]=$temp
done

#Printing array
echo -ne "Sorted array is: "
for((i=0;i<n;i++))
do
    echo -ne "${arr[$i]} "
done

echo ""