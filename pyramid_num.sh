#!/bin/bash

echo "Enter number of rows"

read n

for((i=0;i<n;i++))
do
	num_space=$(($n - $i))

	for((j=0;j<num_space;j++))
	do
		echo -ne " "
	done

	digit=$((2*$i-1))

	half1=$(($digit/2))

	for((j=0;j<digit;j++))
	do
		echo -ne $(($j + 1))
	done

	echo -ne $(($i+1))

	for((j=digit;j>0;j--))
	do
		echo -ne $j
	done
	
	echo ""
done
