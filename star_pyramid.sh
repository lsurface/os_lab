#!/bin/bash

echo "Enter numvber of rows"

read n

for((i=0;i<n;i++))
do
	num_space=$(($n - $i))
	for((j=0;j<num_space;j++))
	do
		echo -ne " "
	done
	
	num_star=$(($i*2 + 1))

	for((j=0;j<num_star;j++))
	do
		echo -ne "*"
	done

	for((j=0;j<num_space;j++))
	do
		echo -ne " "
	done

	echo ""
done
