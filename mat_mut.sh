#!/bin/bash

echo "Enter number of rows for matrix A"
read r1

echo "Enter number of columns for matrix A"
read c1

echo "Enter number of rows for matrix B"
read r2

echo "Enter number of columns for matrix B"
read c2

declare -a A[$(($r1*$c1))]
declare -a B[$(($r2*$c2))]

dim=0
for((i=0;i<r1;i++))
do
	for((j=0;j<c1;j++))
	do
		echo "Enter values for A[$i][$j]"
		read A[$dim]
		dim=$dim+1
	done
done

dim=0
dim=0
for((i=0;i<r2;i++))
do
        for((j=0;j<c2;j++))
        do
                echo "Enter values for B[$i][$j]"
		read B[$dim]
                dim=$dim+1
        done
done

echo "A is"
dim=0
for((i=0;i<r1;i++))
do
        for((j=0;j<c1;j++))
        do
                echo -ne "${A[$dim]} "
                dim=$dim+1
        done
        echo ""
done

echo "B is"
dim=0
for((i=0;i<r2;i++))
do
        for((j=0;j<c2;j++))
        do
                echo -ne "${B[$dim]} "
                dim=$dim+1
        done
        echo ""
done

dim=0
if [ "$c1" -eq "$r2" ]
then
	declare -a C[$(($r1*$c2))]

	for((i=0;i<r1;i++))
	do
		for((j=0;j<c2;j++))
		do
			temp=0
			for((k=0;k<r1;k++))
			do
				C[$(($i*$c2+j))]=$((${C[$(($i*$c2+$j))]}+${A[$(($i*$c1+$k))]}*${B[$(($k*$c2+$j))]}))
			done
			
		done
	done
else
	echo "Matrix dimensions inappropriate for multiplication"
fi

echo "C is"
dim=0
for((i=0;i<r1;i++))
do
        for((j=0;j<c2;j++))
        do
                echo -ne "${C[$dim]} "
                dim=$dim+1
        done
        echo ""
done

