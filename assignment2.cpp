#include<iostream>

using namespace std;

struct process{
    int p_id;
    float burst;
    float arrival;
    float start;
    float completion;
    float waiting;
    float tat;
};

int main(){

    int n;

    cout<<"Enter number of processes\n";
    cin>>n;

    struct process list[n];

    for(int i=0;i<n;i++){
        
        cout<<"\nProcess "<<i<<"\n";
        list[i].p_id=i;
        
        cout<<"Enter Burst time\n";
        cin>>list[i].burst;

        cout<<"Enter Arrival time\n";
        cin>>list[i].arrival;
    }

    float cumulative_time=0;

    for(int i=0;i<n;i++){

        list[i].start=cumulative_time;
        cumulative_time+=list[i].burst;
        list[i].completion=cumulative_time;
        list[i].tat=list[i].completion-list[i].arrival;
        list[i].waiting=list[i].start-list[i].arrival;
    }

    float avg_tat;
    float avg_waiting;

    for(int i=0;i<n;i++){
        avg_tat+=list[i].tat;
        avg_waiting+=list[i].waiting;
    }

    avg_tat=avg_tat/n;
    avg_waiting=avg_waiting/n;

    cout<<"Average Turn Around Time: "<<avg_tat<<"\n";
    cout<<"Average Waiting Time: "<<avg_waiting<<"\n";

    return 0;
}