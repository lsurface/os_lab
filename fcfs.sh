#!/bin/bash

echo "Enter number of processes"
read n

declare -a bt[$n]
declare -a wt[$n]
declare -a tat[$n]

for((i=0;i<n;i++))
do
	echo "Enter burst time for process $i"
	read bt[$i]
done

cum_time=${bt[0]}
wt[0]=0
tat[0]=$((${wt[0]}+${bt[0]}))

for((i=1;i<n;i++))
do
	wt[$i]=$((${wt[$(($i-1))]}+${bt[i-1]}))
	cum_time=$(($cum_time+${bt[$i]}))
	tat[$i]=$((${bt[$i]}+${wt[$i]}))
done


for((i=0;i<n;i++))
do
	echo ""
	echo "Burst time for process $i is ${bt[$i]}"
	echo "Waiting time for process $i is ${wt[$i]}"
	echo "Turn around time for process $i is ${tat[$i]}"
done
