#!/bin/bash

echo "Enter number of rows"
read r

echo "Enter number of columns"
read c
dim=$(($r*$c))
declare -a mat[dim]
dim1=0
for((i=0;i<r;i++))
do

	for((j=0;j<c;j++))
	do
		read mat[$dim1]
		dim1=$(($dim1 + 1))
	done

done
dim1=0
for((i=0;i<r;i++))
do
	for((j=0;j<c;j++))
	do
		echo -ne "${mat[$dim1]} "
		dim1=$(($dim1 + 1))
	done
	
	echo ""
done
