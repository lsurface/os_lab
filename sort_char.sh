#!/bin/bash

arr=(m s t a b)

echo ${arr[@]}

echo -ne "Sorted array is: "

echo $(printf "%s \0" "${arr[@]}" |sort -z |xargs -0n1)

echo -ne "Reverse sorted array is: "
echo $(printf "%s \0" "${arr[@]}" |sort -zr |xargs -0n1)
